# apprentissage de la stack MEVN - Partie 2 - JWT

## Ajout de bootstrap pour le projet

> Afin d'avoir un rendu plus sympatique, on ajoute le CDN bootstrap que l'on copiera dans le fichier "index.html"
> que vous trouverez dans le répertoire "app/public/"

- Pour gagner du temps, voici le lien vers le cdn de [bootstrap (v4.5.0)](https://getbootstrap.com/docs/4.5/getting-started/introduction/).

## Ajout des dépendances recquise pour l'application d'authentification

Dans le répertoire "app", on ajoutera les dépendances suivantes :

- "axios" pour interrargir directement avec la base de donnée
- "vue-router" qui permettra à notre application de gérer le routing
- "vuex" qui nous permettra d'utiliser le store

```sh
> cd app
> pnpm add axios vue-router vuex
```

## Mise en place de quelques composants

> La mise en place de quelques composant est surtout axé pour le bon fonctionnement de l'application. Ainsi nous aurons quelques composants qui seront spécifique à tel ou tel features.

### Architechture (components)

- 📁 app
  - 📁 src
    - 📁 components (à créer)
      - Home.vue (à créer)
      - Signup.vue (à créer)
      - Signin.vue (à créer)
      - TheHeader.vue (à créer)
      - Profile.vue (à créer)
- 📁 build
- 📁 server

> Dans le dossier app/src/ voici la commande pour gagner du temps

```sh
# Toujours dans le répertoire "app/src/"
> mkdir components && cd components && touch Home.vue Signup.vue Signin.vue TheHeader.vue Profile.vue
```

#### contenu de App.vue

```js
<template>
  <div id="app">
    <the-header></the-header>
    <div class="d-flex flex-column w-100">
      <router-view></router-view>
    </div>
  </div>
</template>

<script>
  import TheHeader from "./components/TheHeader";

  export default {
    name: "App",
    components: {
      TheHeader
    }
  }
</script>

<style>
#app {
  font-family: "Avenir", Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
}

html,
body {
  margin: 0;
  padding: 0;
}

.flex-fill {
  flex: auto;
}
</style>
```

#### contenu de Home.vue

```js
<template>
  <div>Home Component</div>
</template>

<script>
export default {
  name: "Home",
  components: {}
};
</script>

<style scoped>
</style>
```

#### contenu de Signup.vue

```js
<template>
  <div>Signup Component</div>
</template>

<script>
export default {
  name: "Signup",
  components: {}
};
</script>
import router
```

#### contenu de Signin.vue

```js
<template>
  <div>Signin Component</div>
</template>

<script>
export default {
  name: "Signin",
  components: {}
};
</script>

<style scoped>
</style>
```

#### contenu de Profile.vue

```js
<template>
  <div>Profile Component</div>
</template>

<script>
export default {
  name: "Profile",
  components: {}
};
</script>

<style scoped>
</style>
```

#### contenu de TheHeader.vue

```js
<template>
  <div class="container-header p-3">
    <router-link to="/" >Dyma</router-link>
    <div class="flex-fill"></div>
    <div class="d-flex flex-row">
      <router-link to="/signin" class="mr-2" >Connexion</router-link>
      <router-link to="/signup" >Inscription</router-link>
    </div>
  </div>
</template>

<script>
export default {
  name: "TheHeader",
  components: {}
};
</script>

<style scoped>
.container-header {
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: #192a56;
  color: white;
}

a {
  color: white;
  text-decoration: none;
}
</style>
```

## Conception du fichier pour le routing

> Pour faire fonctionner vue-router on va utiliser un fichier spécifique qui va traiter le routing.

### Architechture (routes)

- 📁 app
  - 📁 src
    - 📁 routes (à créer)
      - index.js (à créer)
- 📁 build
- 📁 server

> A nouveau dans le dossier app/src/ voici la commande pour gagner encore du temps

```sh
# Toujours dans le répertoire "app/src/"
> mkdir routes && cd routes && touch index.js
```

#### contenu du fichier routes/index.js

```js
import vue from "vue";
import VueRouter from "vue-router";

// Import des composants nécessaire pour le routing
import Home from "./components/Home";
import Signup from "./components/Signup";
import Signin from "./components/Signin";
import Profile from "./components/Profile";

// Middleware
Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    { path "/", component: Home },
    { path "/signup", component: Signup },
    { path "/signin", component: Signin },
    { path "/profile", component: Profile },
  ]
});

export default router;
```

#### Création d'un fichier src/http.js

> le fichier "http.js" va permettre de contrôler l'ensemble des requêtes axios.

```sh
# Dans le dossier src
> touch http.js
```

```js
// contenu du fichier "http.js"
import Vue from "vue";
import axios from "axios";

Vue.prototype.$http = axios;
app;
export default axios;
```

#### Modification du fichier app/main.js

> Dans ce fichier nous allons importer le router fraîchement créer afin de le monter directement. Ainsi lors de l'exécution du rendu, le router sera également passé et disponible partout.

```js
import Vue from "vue";
import App from "./App";
import router from "./routes";
import "./http";

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
```

## Conception du fichier pour le store

> Nous allons utilisé vuex qui dépends d'un store. Pour celà nous allons créer un répertoire dans le dossier "src" appelé "store". dans celui-ci nous allons y ajouter un fichier "index.js"

### Architechture (stores)

- 📁 app
  - 📁 src
    - 📁 store (à créer)
      - index.js (à créer)
- 📁 build
- 📁 server

> Enfin dans le dossier app/src/ voici la commande pour gagner encore du temps

```sh
# Toujours dans le répertoire "app/src/"
> mkdir store && cd store && touch index.js
```

#### contenu du fichier store/index.js

> Contient toute la logique qui sera utilisé pour la conception de l'authentification JWT

```js
import Vue from "vue";
import Vuex from "vuex";
import axios from "./http";
import router from "./routes";

Vue.use(Vuex);

const user = {
  namespaced: true,
  state: {
    // L'utilisateur sera dans le state
    data: {},
    isLoading: false,
    isLoggedIn: null,
    jwtToken: null,
    errors: [],
  },
  getters: {
    // Les getters permettent de récupérer les informations facilement.
    isLoading: (state) => state.isLoading,
    isLoggedin: (state) => state.isLoggedin,
    errors: (state) => state.errors,
    currentUser: (state) => state.date,
    jwtToken: (state) => state.jwtToken,
  },
  actions: {
    // Les actions sont asynchrones afin de répoondre aux futurs requêtes HTTP
    async trySignin(context, credentials) {},
    async trySignup(context, user) {},
    async fetchCurrentUser(context) {},
  },
  mutations: {
    // Les mutations permettrons de modifier le state de façon synchrône
    signupSuccess(state) {},
    signError(state, errors) {},
    signinSuccess(state, data) {},
    signOut(state) {},
  },
};

export default user;
```
