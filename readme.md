# apprentissage de la stack MEVN - Partie 1 - La configuration

## 1 - 3 folders

1. 1er folder va contenir l'application Vue avec le CLI
2. 2eme folder sera l'application vue qui sera build donc minimifié
3. 3eme folder sera le server express

- 📁 app ( sera généré avec vue)
- 📁 build ( sera généré avec "pnpm run build" mais pour les tests ont peut le créer à la main avec un index.html bidon pour tester)
- 📁 server (sera généré avec express)

## 1 installer express-generator

```sh
> sudo pnpm install -g express-generator
```

### Génération de l'ossature du server express (3ème folder)

> L'étape 5 est inutile vu que pug ne sera pas utilisé. En revanche c'est toujours bon de savoir par quoi on remplace jade. ;)
> Ce qu'il faut comprendre c'est que JADE à été renommé en PUG suite à un problème de droit d'auteur.

```sh
# étape 1 : Génération du server
> express server

# étape 2 : Se déplacer dans le 3ème folder fraîchement créé (server)
> cd server

# étape 3 : Installer les dépendances recquise.
> pnpm install

# étape 4 : Cette version vous fera installer JADE qui est déprécié, donc on supprime JADE.
> pnpm remove jade

# étape 5 : Remplacement de JADE par PUG
> pnpm add pug
```

## 2 Modification du serveur pour faire en sorte qu'il fonctionne comme une API

### Fichier app.js généré grâce à "express server" (étape 1)

```js
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
```

### Fichier app.js modifié

```js
const express = require("express"); // Server Web
const path = require("path");

const app = express(); // Ici, on créer l'application express

// Utilisation de plusieurs middleware (La logique qui va se lancé au lancement de l'application)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "../build")));
// client-build correspond au 2ème folder

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "../build/index.html"));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json("error");
});

module.exports = app;
```

## 3 Suppression des dossiers inutiles du server express

- 📁 public (A supprimer)
- 📁 routes (A supprimer)
- 📁 views (A supprimer)

## 4 lancer le serveur pour tester la connexion

```sh
> cd server
> pnpm start
```

> Reste plus qu'à se rendre sur l'URL localhost:3000 pour voir le contenu du fichier index.htmlapp

## 5 Installation d'un serveur mongoDB

> il faut se rendre sur [mongoDB](https://account.mongodb.com/account)
> Une fois que tout es créer on cherche et on copie l'URL pour se connecter à mongoose soit :
> mongodb+srv://<pseudo>:\<password>@adresse.mongodb.net/\<dbname>?retryWrites=true&w=majority

### Installation d'un ORM pour se connecter à la base de donnée + la dépendance colors

> Toujours dans le 3èmes folder (server)

```sh
> pnpm add mongoose colors
```

### Conception d'un répertoire "db" avec un fichier "index.js"

- 📁 server
- 📁 db => index.js

> Ce répertoire permet de dissocier la connexion à une quelconque base de donnée.
> En effet si on passe en claire les identifiants, ils serait visible à tout le monde et la sécurité de l'application serait elle compromise.

#### Contenu du fichier db/index.js

> Ce fichier étant a part, il ne sera en aucun cas indexé pour les raisons évoqué ci-dessus.

```js
require("colors"); // Permet d'avoir juste de la couleur dans le terminal
// Début ajout de la partie mongoose
const mongoose = require("mongoose");

mongoose
  .connect(
    "mongodb+srv://test:123@mevn.c7ya2.mongodb.net/chap24vue?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("Connexion OK".brightGreen))
  .catch((err) => console.log(err));
// Fin de la partie mongoose
```

#### Modification du fichier server/app.js

```js
require("./db"); // Ajout de la connexion à la base de donnée.
const express = require("express");
const path = require("path");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "../client-build")));

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "../client-build/index.html"));
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  res.status(err.status || 500);
  res.json("error");
});

module.exports = app;
```

## Edition du package.json

> Ajout d'un script pour la production et donc permettre l'utilisation du PORT 80
> Sur mon vps j'aurais juste à lancer : "pnpm run prod"

````json
{
  "...": "du code avant...",

  "scripts": {
    "start": "node ./bin/www",
    "prod": "PORT=80 node./bin/www"
  },

  "...": "du code après"
}

#### Test rapide que le server fonctionne bien

```sh
> pnpm start
````

> Une fois le serveur démarrer, nous devrions avoir connexion OK d'afficher en vert dans le terminal.

## Dernière étape création de l'application client (répertoire app)

> Pour celà on va utiliser le CLI pour créer un dossier "client" avec toute l'application Vue de base. A la racine je saisie la commande :
> A la racine j'ai mon dossier client-build et server.

```sh
> vue create app
```

> A ce moment il faut créer dans le dossier client un fichier de configuration pour vue. (Le tout dans 1er dossier "client")

```sh
> cd app
> touch vue.config.js
```

### Le code du fichier de vue.config.js

```js
module.exports = {
  productionSourceMap: false,
  devServer: {
    proxy: "http://localhost:3000",
  },
  outputDir: "../build",
};
```

## Modification de l'application VUE

> A cet instant, vous pouvez concevoir votre application ou bien laisser celle générée automatiquement afin d'allez plus vite.
> Elle sera de toute manière éditer lors de l'utilisation de JWT

## Build de l'application

> Sans modification du code quelle qu'il soit, nous nous rendons dans le dossier "app" et nous lançons dans le terminal la commande permettant de build l'application
> (en sommes le contenu du dossier "build" sera créé automatiquement)

```sh
> cd app
> pnpm run build
```

> A ce moment nous pouvons contrôler que le server est toujours actif et que lorsqu'on se rends sur l'adresse en question nous nous retrouvons bien avec la page de Vue optimisé !!

## suppresion du répertoire .git du dossier "app".

> En effet on va avoir un conflit vu que Vue create crée automatiquement un répertoire .git

```sh
> cd app

# voir que le repertoire .git existe
> ls -a

> rm -rf .git
```

## création d'un fichier .gitignore

```sh
# On remonte à la racine (app / build / server)
> cd ..
> touch .gitignore
```

### contenu du fichier .gitignore

```git
app/node_modules
server/node_modules
# suppression des identitifiants de connexion
server/db/index.js
```

## initialisation de git dans le répertoire root

```sh
> git init
```

## ajout des fichier dans la staging aréa

```sh
> git add .
> git commit -m "first commit"
```

> On colle l'adresse du répertoire distant git que l'on récupère sur github ou gitlab, ça ressemble à ça :

git remote add origin git@gitlab.com:Zyrass/vue-jwt-vps.git

> puis ont termine avec un git push

```sh
> git push -u origin master
```

## Connexion à ssh

> Là, vous devez vous connecté en ssh à votre server, une fois connecté, il vous faudra vous rendre dans le dossier server vue
> qu'il faudra bien entendu recréer le répertoire "db" ainsi que le fichier "index.js" avant de lancer n'importe quelle commande.
> Pour ce faire, rien de plus simple. Une fois dans le répertoire "server" il faudra saisir cette commande :

```sh
# mkdir pour la création du répertoire db
# cd pour se rendre dans db
# touch index.js pour créer le fichier index.js
# sudo nano index.js pour ouvrir nano et éditer le fichier
> mkdir db && cd db && touch index.js && sudo nano index.js
```

> La il suffira simplement de copier coller le contenu de notre fichier index.js qu'on a en local. On sauvegarde et l'affaire sera réglé.

### lancer le server

```sh
> cd ..
> cd server
> pnpm run prod
```
